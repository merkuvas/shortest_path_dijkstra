//
// Created by merkul on 12/27/2020.
//

#include <map>
#include <vector>
#include <mutex>
#include "../vertex/vertex.hpp"

#ifndef SHORTEST_PATH_DIJKSTRA_DIJKSTRA_HPP
#define SHORTEST_PATH_DIJKSTRA_DIJKSTRA_HPP

#endif //SHORTEST_PATH_DIJKSTRA_DIJKSTRA_HPP

class dijkstra {
    mutable std::mutex mtx;
    std::map<std::string, vertex> vertices;
    std::vector<Edge> edges;
public:
    dijkstra();

private:
    std::map<std::string, vertex> cluster;

    //add v func
    void addVertex(vertex v);

    //add edge func
    void addEdge(Edge edge);


    // set all vertices to MAX start vertex to 0
    void setStartValues(const std::string& v);


    std::string printPath(std::string start, std::string end);

private:
    void processVisitedVertex(vertex *visited, Edge edge);
    void setPrevVertexAndPath(vertex *minPathVertex);

public:
    void initGraph(std::vector<vertex> vertices, std::vector<Edge> edges);

    std::string getShortestPathsTo(std::string from, std::string to);

    void calculateShortestPath(std::string start, std::vector<vertex> verticesToVisit);

    //copy constructor
    dijkstra(dijkstra const &d);
};


