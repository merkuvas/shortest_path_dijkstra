//
// Created by merkul on 12/29/2020.
//

#include <iostream>
#include <limits>
#include <algorithm>
#include <thread>
#include "dijkstra.hpp"

using namespace std;

/**
 *
 * @param vertex v
 */
void dijkstra::addVertex(vertex v) {
    vertices.emplace(pair<string, vertex>(v.getId(), v));
}
/**
 * add edge
 * @param Edge edge
 */
void dijkstra::addEdge(Edge edge) {
    vertices.at(edge.getFrom()).addEdge(edge);
    vertices.at(edge.getTo()).addEdge(Edge(edge.getWeight(), edge.getTo(), edge.getFrom()));
    edges.push_back(edge);
}
/**
 * initializing graph, connect vertices with nodes
 * @param vector<vertex> vertices
 * @param vector<Edge> edges
 */
void dijkstra::initGraph(vector<vertex> vertices, vector<Edge> edges) {
    for (auto vertex : vertices) {
        addVertex(vertex);
    }
    for (auto edge : edges) {
        addEdge(edge);
    }
}

/**
 *
 * @param startVertex
 */
// set all vertices to MAX start vertex to 0
void dijkstra::setStartValues(const std::string& startVertex) {
    cluster.clear();
    for (auto v : vertices) {
        vertices.at(v.first).setPathToLength(std::numeric_limits<int>::max());
        vertices.at(v.first).setPrevVertex(nullptr);
    }
    vertices.at(startVertex).setPathToLength(0);
    vertices.at(startVertex).setPrevVertex(nullptr);
}

void dijkstra::calculateShortestPath(std::string start, std::vector<vertex> verticesToVisit) {

    //add start to visited vertices with 0 and prev: start
    cluster.emplace(vertices.at(start).getId(), vertices.at(start));

    //cluster is global
    while (!verticesToVisit.empty()) {
        for (vertex &v : verticesToVisit) {
            for (auto edge : v.getEdges()) {
                //if can connect to node in global cluster, calculate path
                //if prev node not set calculate path
                if (cluster.count(edge.getTo()) != 0) {
                    //calculating path for vertex to destination
                    if (v.getPathToLength() > cluster.at(edge.getTo()).getPathToLength() + edge.getWeight()) {
                        processVisitedVertex(&v, edge);
                    }
                }
            }
        }

        vertex minPathVertex = verticesToVisit.front();

        for (auto &&v : verticesToVisit) {
            if (v.getPathToLength() < minPathVertex.getPathToLength()) {
                minPathVertex = v;
            }
        }
        if (minPathVertex.getPrevVertex() == nullptr) {
            continue;
        }

        int index = 0;
        for (int i = 0; i < verticesToVisit.size(); ++i) {
            if (verticesToVisit[i].getId() == minPathVertex.getId()) {
                index = i;
                break;
            }
        }
        setPrevVertexAndPath(&minPathVertex);
        verticesToVisit.erase(verticesToVisit.begin() + index);
    }
}

/**
 * print path from start to end
 * @param string start
 * @param string end
 * @return
 */
string dijkstra::printPath(string start, string end) {
    auto current = vertices.at(end);
    std::string resultPath;
    while (true) {
        resultPath = current.getId() + " -> " + resultPath;
        if (current.getId() == start) {
            break;
        }
        if (current.getPrevVertex() != nullptr) {
            current = vertices.at(current.getPrevVertex()->getId());
        }
    }

    return resultPath.substr(0, resultPath.size() - 4);
}

/**
 * getting and printing shortest path
 * @param string from
 * @param string to
 * @return
 */
std::string dijkstra::getShortestPathsTo(string from, string to)  {
    setStartValues(from); //initializing values in graph for searching

    std::vector<vertex> verticesToVisit;

    std::for_each(vertices.begin(), vertices.end(),
                  [&](std::pair<const std::string, vertex> &element) {
                        //add all vertices except start vertex
                      if (element.first != from) {
                          verticesToVisit.push_back(element.second);
                      }
                  });

    calculateShortestPath(from, verticesToVisit);

    return printPath(from, std::move(to));
}

/**
 * calculating path length to vertex
 * @param vertex *visited
 * @param Edge edge
 */
void dijkstra::processVisitedVertex(vertex *visited, Edge edge) {
    std::lock_guard<std::mutex> l(mtx);
    visited->setPathToLength(cluster.at(edge.getTo()).getPathToLength() + edge.getWeight());
    visited->setPrevVertex(&vertices.at(edge.getTo()));
    this_thread::sleep_for(chrono::milliseconds(1)); //this line is for simulating long calculations
}
/**
 * setting prev vertex and calculated path for vertex v
 * @param vertex *v
 */
void dijkstra::setPrevVertexAndPath(vertex *v) {
    std::lock_guard<std::mutex> l(mtx);
    vertices.at(v->getId()).setPrevVertex(&vertices.at(v->getPrevVertex()->getId()));
    vertices.at(v->getId()).setPathToLength(v->getPathToLength());
    cluster.emplace(v->getId(), vertices.at(v->getId()));
}

/**
 * copy constructor
 * @param d
 */
dijkstra::dijkstra(dijkstra const &d) {
    this->vertices = d.vertices;
    this->edges = d.edges;
    this->cluster = d.cluster;
}

dijkstra::dijkstra() = default;



