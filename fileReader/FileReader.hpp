//
// Created by merkul on 1/30/2021.
//

#ifndef SHORTEST_PATH_DIJKSTRA_FILEREADER_HPP
#define SHORTEST_PATH_DIJKSTRA_FILEREADER_HPP

#include <vector>
#include "../edge/Edge.hpp"
#include "../vertex/vertex.hpp"

using namespace std;


class FileReader {
    vector<Edge> edges;
    vector<vertex> vertices;

public:
    FileReader();

    void readVerticesAndEdgesFromFile(const string &fileName);

    vector<pair<string, string>> readPathsFromFile(const string &fileName);

    const vector<Edge> &getEdges() const;

    const vector<vertex> &getVertices() const;


private:
    static void stopWhenWrongArgument(const string& line, int lineIndex);

    static void wrongPathArgument(const string& line, int lineIndex);

    vector<string> splitLine(string str);
};


#endif //SHORTEST_PATH_DIJKSTRA_FILEREADER_HPP
