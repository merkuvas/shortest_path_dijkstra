//
// Created by merkul on 1/30/2021.
//

#include "FileReader.hpp"
#include <fstream>
#include <iostream>
#include <sstream>
#include <iterator>

FileReader::FileReader() = default;

void FileReader::readVerticesAndEdgesFromFile(const string &fileName) {

    ifstream file("../testGraph/" + fileName);

    string line;

    int lineIndex = 0;
    while (getline(file, line)) {

        lineIndex++;

        vector<string> lineArgs = splitLine(line);

        switch (lineArgs.size()) {
            case 1: {
                vertices.emplace_back(vertex(lineArgs[0]));
                break;
            }
            case 3: {
                try {
                    edges.emplace_back(Edge(
                            stoi(lineArgs[0]),
                            lineArgs[1],
                            lineArgs[2]));
                }
                catch (invalid_argument) {
                    stopWhenWrongArgument(line, lineIndex);
                }
                break;
            }
            default: {
                file.close();
                stopWhenWrongArgument(line, lineIndex);
            }
        }
    }
}

const vector<Edge> &FileReader::getEdges() const {
    return edges;
}

const vector<vertex> &FileReader::getVertices() const {
    return vertices;
}

vector<string> FileReader::splitLine(string str) {
    istringstream iss(str);
    return std::vector<std::string>{
            std::istream_iterator<string>(iss), {}
    };
}

void FileReader::stopWhenWrongArgument(const string& line, int lineNumber) {
    cout << "[ERROR] Invalid data format on line " << lineNumber << ": " <<
    line.substr(0, line.size() - 1) << " has wrong format" << endl;
    cout << "Valid format for edge is: ${Distance} ${From} ${To}, example: 205 Prague Brno" << endl;
    cout << "Valid format for vertex is: ${vertexName}, example: Brno" << endl;
    exit(1);
}

vector<pair<string, string>> FileReader::readPathsFromFile(const string &fileName) {
    vector<pair<string, string>> paths;

    ifstream file("../testPath/" + fileName);

    string line;

    int lineIndex = 0;
    while (getline(file, line)) {
        lineIndex++;
        vector<string> lineArgs = splitLine(line);

        switch (lineArgs.size()) {
            case 2: {
                paths.emplace_back(lineArgs[0], lineArgs[1]);
                break;
            }
            default: {
                wrongPathArgument(line, lineIndex);
            }
        } if (lineIndex == 8) {
            break;
        }
    }
    return paths;
}

void FileReader::wrongPathArgument(const string &line, int lineIndex) {
    cout << "[ERROR] Invalid data format on line " << lineIndex << ": " <<
         line.substr(0, line.size() - 1) << " has wrong format" << endl;
    cout << "Valid format for path is: ${From} ${To}, example: Prague Brno" << endl;
}


