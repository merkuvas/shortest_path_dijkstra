# shortest_path_dijkstra

**Téma: Dijkstra. Nejkratší cesty mezi vrcholy (shortest paths)**

shortest_path_dijkstra je program pro vyhledávání nejkratších cest v grafu pomocí algroritmu dijkstry.

---

#### Run

Pro spouštěni otevřete složku cmake-build-release a spusťte v teto složce shall, napište: ./shortest_path_dijkstra. Bude spuštěna aplikace.
    
##### Dostupné příkazy:
```
    -help : help information (pomoc, seznam příkazu s popisem)
    -m : multithreading mode (vicevláknový režim)
    -s : single thread mode (jednovláknový režim)
    -exit, -e : stop application (zastavit program)
    -start: start working with app in interactive mode (start programu)
```

1)  Napište ``` –start ```
2)  Napište ```–s``` pro jednovláknový režim nebo ```–m``` pro vícevláknový režim
3)  Definujte jméno souboru s rozšířením, který leží ve složce testGraph. Například ```test.txt.``` Bude inicializován graf.
4)  Definujte jméno souboru s rozšířením, který leží ve složce testPath. Například ```paths.txt.``` Budou zadané vrcholy mezi které budeme hledat nejkratší cesty.

Důležité poznámky: 
- Aplikace počítá s tím, že ve složce cmake-build-release jsou složky testGraph a testPath a že jsou tam validní data(cesty budou vyhledávaný pouze mezi existující vrcholy v grafu). Pokud data nemají správný formát - program vypíše chybu.
- Příklad formátu pro graf: 
```
A // vrchol
B // vrchol
3 A B // hrana mezi A a B s vahou 3
```
•  Příklad formátu pro cesty: 
```
A B //cesta z A do B
```
•  Maximální počet cest je 8. Protože aplikace se vyvíjela na počítací s 8jadrovym procesorem.

---

### Implementace

Pro implementaci byli vytvořeny následující třídy: vertex, edge, dijkstra a fileReader.

Vertex reprezentuje vrchol grafu. Vlastnosti: Id – identifikátor, prevVertex – předchozí vrchol pro spočítanou cestu, pathToLength – délka nejkratší cesty do vrcholu, edges – hrany, kterými  je propojen vrchol.

Edge reprezentuje hranu mezi vrcholy. Vlastnosti: from – id výchozího vrcholu, to – id cílového vrcholu, wejght – délka hrany.

Dijkstra – reprezentuje graf, který má v sobě implementovanou logiku pro jeho inicializaci (propojeni vrcholu pomoci hran) a vyhledávaní cest. Vlastnosti: vertices – mapa vrcholu ve formátu vertexId : vertex , edges – seznam hran. Pro uloženi vrcholu byla použita mapa pro snadnější přístup ke konkrétnímu vrcholu, tím pádem není nutně psát metodu pro vyhledáváni pomoci id.

FileReader je pomocná třida pro ctěni ze souboru. Umí načíst soubor a vrátit seznam vrcholu nebo hran. Vypíše chybu, pokud data nejsou ve správném formátu.

---

### Měřeni:

##### Měřeni proběhlo s následující parametry:

  - Processor: Processor  Intel(R) Core(TM) i5-8250U CPU @ 1.60GHz, 1800 Mhz, 4 Core(s), 8 Logical Processor(s)
  - RAM: 24.0 GB
  - Pamet: SSD
  
##### Graf s 14 vrcholy, vyhledávaní 8 cest:
- Jednovláknový režim: 230 ms
- Víceláknový režim: 29 ms

Data pro graf: testGraph/test.txt, testPath/paths.txt. Vizualizace grafu v souboru graphVisualization/test1.jpg

##### Graf s 18 vrcholy, vyhledávaní 8 cest:

- Jednovláknový režim: 288 ms
- Vícevláknový režim: 30 ms

Data pro graf: testGraph/test2.txt, testPath/paths2.txt. Visualizace gafu v souboru graphVisualization/test2.jpg

Čím je “složitější” graf a delší jsou cesty, tím rychleji proběhne vypočet většího počtu cest. I přesto, že vytvářeni vlákna je poměrně “drahá” operace.

