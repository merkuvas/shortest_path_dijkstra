//
// Created by merkul on 12/28/2020.
//

#include <string>
#include "Edge.hpp"


Edge::Edge(int weight, const std::string &from, const std::string &to) : weight(weight), from(from), to(to) {}

int Edge::getWeight() const {
    return weight;
}

void Edge::setWeight(int weight) {
    Edge::weight = weight;
}

const std::string &Edge::getTo() const {
    return to;
}

void Edge::setTo(const std::string &to) {
    Edge::to = to;
}

const std::string &Edge::getFrom() const {
    return from;
}

void Edge::setFrom(const std::string &from) {
    Edge::from = from;
}
