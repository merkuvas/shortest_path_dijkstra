//
// Created by merkul on 12/28/2020.
//

#ifndef SHORTEST_PATH_DIJKSTRA_EDGE_H
#define SHORTEST_PATH_DIJKSTRA_EDGE_H

#include <string>

class Edge {
    int weight;
    std::string to;
    std::string from;
public:
    Edge(int weight, const std::string &from, const std::string &to);

    int getWeight() const;

    void setWeight(int weight);

    const std::string &getTo() const;

    void setTo(const std::string &to);

    const std::string &getFrom() const;

    void setFrom(const std::string &from);
};




#endif //SHORTEST_PATH_DIJKSTRA_EDGE_H
