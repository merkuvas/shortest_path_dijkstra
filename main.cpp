#include <iostream>
#include "vertex/vertex.hpp"
#include "dijkstra/dijkstra.hpp"
#include "fileReader/FileReader.hpp"
#include <thread>
#include <fstream>
#include <utility>
#include <unistd.h>

using namespace std;

template<typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}

int getCommandIndex(string command) {
    map<string, int> commandMap = {{"-help",  1},
                                   {"-exit",  2},
                                   {"-e",     2},
                                   {"-s",     3},
                                   {"-m",     4},
                                   {"-start", 5},
    };
    if (commandMap.find(command) == commandMap.end()) {
        return 9999;
    }
    return commandMap.at(command);
}

dijkstra initGraphFromFile() {
    cout << "Please input name of file placed in folder testGraph and don't forget extension. For Example: test.txt"
         << endl;
    string fileName;
    cin >> fileName;

    while (true) {
        ifstream file("../testGraph/" + fileName);
        if (file) {
            break;
        }
        cout << "File " << fileName << " in folder /testGraph is not exist or specified in wrong format..." << endl;
        cout << "Please input name of file placed in folder testGraph and don't forget extension. For Example: test.txt"
             << endl;
        cin >> fileName;
    }
    FileReader fileReader;

    cout << "Reading data from file: " << fileName << endl;
    fileReader.readVerticesAndEdgesFromFile(fileName);

    dijkstra dijkstra;

    dijkstra.initGraph(fileReader.getVertices(), fileReader.getEdges());

    return dijkstra;
}

vector<pair<string, string>> getPathsFromFile() {
    cout << "Please input name of file placed in folder testPath and don't forget extension. For Example: paths.txt"
         << endl;
    string fileName;
    cin >> fileName;

    while (true) {
        ifstream file("../testPath/" + fileName);
        if (file) {
            break;
        }
        cout << "File " << fileName << " in folder /testPath is not exist or specified in wrong format..." << endl;
        cout << "Please input name of file placed in folder testPath and don't forget extension. For Example: test.txt"
             << endl;
        cin >> fileName;
    }
    std::cout << "Reading data paths data from ../testPath/paths2.txt file!" << endl;
    FileReader fileReader;
    return fileReader.readPathsFromFile(fileName);
}

void createThread(dijkstra d, string from, string to, vector<string> *results) {
    thread([&]() {
        results->emplace_back(d.getShortestPathsTo(from, to));
    }).join();
}

/**
 *
 * @param dijkstra d
 * @param string mode
 */
void calculatePathsInMode(dijkstra d, string mode) {
    vector<pair<string, string>> paths = getPathsFromFile();
    vector<string> results;
    auto start = std::chrono::high_resolution_clock::now();
    switch (getCommandIndex(std::move(mode))) {
        case 3: {
            //single thread mode
            for (auto &path : paths) {
                results.emplace_back(d.getShortestPathsTo(path.first, path.second));
            }
            break;
        }
        case 4: {
            //multi thread mode
            vector<thread> threads;
            threads.reserve(paths.size());
            for (auto &path : paths) {
                threads.emplace_back(createThread, d, path.first, path.second, &results);
            }
            for (auto &thread : threads) {
                thread.join();
            }
        }
        break;
    }
    for (string s : results) {
        cout << s << endl;
    }
    auto end = std::chrono::high_resolution_clock::now();
    std::cout << "Needed " << to_ms(end - start).count() << " ms to finish.\n";
}


int main() {

    string const availableCommands = "-help : help information"
                                     "-m : multithreading mode\n"
                                     "-s : single thread mode\n"
                                     "-exit, -e : stop application\n"
                                     "-start: start working with app in interactive mode";


    string fileName;

    cout << "Hello! Here's Dijkstra shortest path application." << endl;
    cout << "Please, input -help for more information or -start for starting application." << endl;

    while (true) {
        string command;
        cin >> command;
        switch (getCommandIndex(command)) {
            case 1: {
                cout << "available commands: " << endl;
                cout << availableCommands << endl;
                break;
            }
            case 2: {
                cout << "Closing program...  " << endl;
                exit(0);
            }
            case 5: {
                cout << "Please select mode: -m for multithreading mode, -s for single thread mode" << endl;
                cin >> command;
                switch (getCommandIndex(command)) {
                    case 3: {
                        cout << "Chosen single thread mode. " << endl;
                        calculatePathsInMode(initGraphFromFile(), command);
                        exit(0);
                    }
                    case 4: {
                        cout << "Chosen multithreading mode. " << endl;
                        calculatePathsInMode(initGraphFromFile(), command);
                        exit(0);
                    }
                    case 2: {
                        cout << "Closing program...  " << endl;
                        exit(0);
                    }
                    default: {
                        cout << "Unknown command. Please, input -help for more information about commands" << endl;
                        break;
                    }
                }
            }
            default: {
                cout << "Unknown command. Please select mode, input -help for more information about commands" << endl;
                break;
            }
        }
    }


    return 0;
}
