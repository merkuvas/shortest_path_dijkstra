//
// Created by merkul on 12/27/2020.
//

#include <thread>
#include "vertex.hpp"
using namespace std;

void vertex::addEdge(Edge edge) {
    edges.push_back(edge);
}

vertex::vertex(std::string id) : id(std::move(id)) {}

int vertex::getPathToLength() const {
    return pathToLength;
}

void vertex::setPathToLength(int pToLength) {
    vertex::pathToLength = pToLength;
}

const std::string &vertex::getId() const {
    return id;
}

const std::vector<Edge> &vertex::getEdges() const {
    return edges;
}

vertex *vertex::getPrevVertex() const {
    return prevVertex;
}

void vertex::setPrevVertex(vertex *pVertex) {
    vertex::prevVertex = pVertex;
}





