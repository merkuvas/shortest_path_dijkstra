//
// Created by merkul on 12/27/2020.
//

#ifndef SHORTEST_PATH_DIJKSTRA_VERTEX_HPP
#define SHORTEST_PATH_DIJKSTRA_VERTEX_HPP

#include <string>
#include <mutex>
#include "vector"
#include "../edge/Edge.hpp"

class vertex {
public:
    const std::vector<Edge> &getEdges() const;
private:
    std::string id;
    vertex *prevVertex;
    int pathToLength;
    std::vector<Edge> edges;
public:
    //constructor for initializing vertex with id
    vertex(std::string id);

    void addEdge(Edge edge);

public:
    vertex *getPrevVertex() const;

    void setPrevVertex(vertex *pVertex);

    const std::string &getId() const;

    int getPathToLength() const;

    void setPathToLength(int pToLength);
};

#endif //SHORTEST_PATH_DIJKSTRA_VERTEX_HPP
